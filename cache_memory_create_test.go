package icache_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/icache"
)

func TestCacheMemoryCreate(t *testing.T) {
	cache := icache.NewMemoryCache(time.Second)

	id, err := cache.Create(context.Background(), []byte("rcache-test"))

	assert.NoError(t, err, "[TestCacheMemoryCreate] Should not error")
	assert.NotEmpty(t, id, "[TestCacheMemoryCreate] Should not empty")
}
