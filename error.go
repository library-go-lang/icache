package icache

import "fmt"

var (
	ErrNotFound  error = fmt.Errorf("key not found")
	ErrNoContent error = fmt.Errorf("content is empty")
)
