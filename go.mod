module gitlab.com/library-go-lang/icache

go 1.17

require (
	github.com/go-redis/redis/v8 v8.11.4
	github.com/stretchr/testify v1.7.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/oklog/ulid/v2 v2.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	gitlab.com/library-go-lang/iencryption v0.1.1 // indirect
	gitlab.com/library-go-lang/ihelper v0.0.0-20230911085606-ce3b957367bd
	go.mongodb.org/mongo-driver v1.8.3 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)

require golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
