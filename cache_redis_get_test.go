package icache_test

import (
	"context"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/library-go-lang/icache"
	"gitlab.com/library-go-lang/icache/mocks"
)

func TestCacheRedisGetSuccess(t *testing.T) {
	bts := new(mocks.RCacheRedisStatusCmd)
	bts.On("Bytes").
		Return([]byte("rcache-test"), nil)

	client := new(mocks.RCacheRedisClient)

	client.On("Get", mock.Anything, mock.Anything).
		Return(bts)

	cache := icache.NewRedisCache(client, time.Second)

	b, err := cache.Get(context.Background(), "tes")

	assert.NoError(t, err, "[TestCacheRedisGetSuccess] Should not error")
	assert.NotEmpty(t, b, "[TestCacheRedisGetSuccess] Should not empty")
	assert.Equal(t, []byte("rcache-test"), b, "[TestCacheRedisGetSuccess] Should equal []byte(\"rcache-test\")")
}

func TestCacheRedisGetErrorNotFound(t *testing.T) {
	bts := new(mocks.RCacheRedisStatusCmd)
	bts.On("Bytes").
		Return(nil, redis.Nil)

	client := new(mocks.RCacheRedisClient)

	client.On("Get", mock.Anything, mock.Anything).
		Return(bts)

	cache := icache.NewRedisCache(client, time.Second)

	b, err := cache.Get(context.Background(), "tes")

	assert.Error(t, err, "[TestCacheRedisGetErrorNotFound] Should error")
	assert.Equal(t, icache.ErrNotFound, err, "[TestCacheRedisGetErrorNotFound] Error should \"key not found\"")
	assert.Empty(t, b, "[TestCacheRedisGetErrorNotFound] Should empty")
}

func TestCacheRedisGetErrNoContent(t *testing.T) {
	bts := new(mocks.RCacheRedisStatusCmd)
	bts.On("Bytes").
		Return([]byte{}, nil)

	client := new(mocks.RCacheRedisClient)

	client.On("Get", mock.Anything, mock.Anything).
		Return(bts)

	cache := icache.NewRedisCache(client, time.Second)

	b, err := cache.Get(context.Background(), "tes")

	assert.Error(t, err, "[TestCacheRedisGetErrNoContent] Should error")
	assert.Equal(t, icache.ErrNoContent, err, "[TestCacheRedisGetErrNoContent] Error should \"content is empty\"")
	assert.Empty(t, b, "[TestCacheRedisGetErrNoContent] Should empty")
}
