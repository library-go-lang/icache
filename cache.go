package icache

import (
	"context"
	"time"
)

type CacheGetter interface {
	Get(ctx context.Context, key string) (value []byte, e error)
}

type CacheCreater interface {
	Set(ctx context.Context, key string, value []byte) (e error)
	Create(ctx context.Context, value []byte) (key string, e error)
	Delete(ctx context.Context, key string) (e error)
}

type CacheCreateDuration interface {
	SetWithDuration(ctx context.Context, key string, value []byte, dur time.Duration) (e error)
	CreateWithDuration(ctx context.Context, value []byte, dur time.Duration) (key string, e error)
}

type Cache interface {
	CacheGetter
	CacheCreater
	CacheCreateDuration
}

type CacheCloser interface {
	Cache
	Close() (err error)
}
