package icache_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/library-go-lang/icache"
	"gitlab.com/library-go-lang/icache/mocks"
)

func TestCacheRedisDelSuccess(t *testing.T) {
	cmd := new(mocks.RCacheRedisErrorCmd)
	cmd.On("Err").
		Return(nil)

	client := new(mocks.RCacheRedisClient)

	client.On("Del", mock.Anything, mock.Anything).
		Return(cmd)

	cache := icache.NewRedisCache(client, time.Second)

	err := cache.Delete(context.Background(), "tes")

	assert.NoError(t, err, "[TestCacheRedisDelSuccess] Should not error")
}

func TestCacheRedisDelError(t *testing.T) {
	cmd := new(mocks.RCacheRedisErrorCmd)
	cmd.On("Err").
		Return(fmt.Errorf("tes"))

	client := new(mocks.RCacheRedisClient)

	client.On("Del", mock.Anything, mock.Anything).
		Return(cmd)

	cache := icache.NewRedisCache(client, time.Second)

	err := cache.Delete(context.Background(), "tes")

	assert.Error(t, err, "[TestCacheRedisDelError] Should error")
	assert.Equal(t, fmt.Errorf("tes"), err, "[TestCacheRedisDelError] Error should \"tes\"")
}
