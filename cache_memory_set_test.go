package icache_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/icache"
)

func TestCacheMemorySet(t *testing.T) {
	cache := icache.NewMemoryCache(time.Second)

	err := cache.Set(context.Background(), "tes", []byte("rcache-test"))

	assert.NoError(t, err, "[TestCacheMemorySet] Should not error")
}
