package icache_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/icache"
	"gitlab.com/library-go-lang/icache/mocks"
)

func TestCacheRedisCloseSuccess(t *testing.T) {
	client := new(mocks.RCacheRedisClient)

	client.On("Close").
		Return(nil)

	cache := icache.NewRedisCache(client, time.Second)

	err := cache.Close()

	assert.NoError(t, err, "[TestCacheRedisCloseSuccess] Should not error")
}

func TestCacheRedisCloseError(t *testing.T) {
	client := new(mocks.RCacheRedisClient)

	client.On("Close").
		Return(fmt.Errorf("tes"))

	cache := icache.NewRedisCache(client, time.Second)

	err := cache.Close()

	assert.Error(t, err, "[TestCacheRedisCloseError] Should error")
	assert.Equal(t, fmt.Errorf("tes"), err, "[TestCacheRedisCloseError] Error should \"tes\"")
}
