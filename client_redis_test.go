package icache_test

import (
	"context"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/icache"
)

func TestCacheRedisClientGet(t *testing.T) {
	opts := &redis.Options{
		Addr:         "",
		Username:     "",
		Password:     "",
		DialTimeout:  time.Second * 1,
		ReadTimeout:  time.Second * 1,
		WriteTimeout: time.Second * 1,
		DB:           0,
	}

	cache := icache.NewRedisClient(opts)

	cmd := cache.Get(context.Background(), "tes")

	assert.NotEmpty(t, cmd, "[TestCacheRedisClientGet] Should not empty")
}

func TestCacheRedisClientDel(t *testing.T) {
	opts := &redis.Options{
		Addr:         "",
		Username:     "",
		Password:     "",
		DialTimeout:  time.Second * 1,
		ReadTimeout:  time.Second * 1,
		WriteTimeout: time.Second * 1,
		DB:           0,
	}

	cache := icache.NewRedisClient(opts)

	cmd := cache.Del(context.Background(), "tes")

	assert.NotEmpty(t, cmd, "[TestCacheRedisClientDel] Should not empty")
}

func TestCacheRedisClientSet(t *testing.T) {
	opts := &redis.Options{
		Addr:         "",
		Username:     "",
		Password:     "",
		DialTimeout:  time.Second * 1,
		ReadTimeout:  time.Second * 1,
		WriteTimeout: time.Second * 1,
		DB:           0,
	}

	cache := icache.NewRedisClient(opts)

	cmd := cache.Set(context.Background(), "tes", "", time.Second*5)

	assert.NotEmpty(t, cmd, "[TestCacheRedisClientSet] Should not empty")
}

func TestCacheRedisClientClose(t *testing.T) {
	opts := &redis.Options{
		Addr:         "",
		Username:     "",
		Password:     "",
		DialTimeout:  time.Second * 1,
		ReadTimeout:  time.Second * 1,
		WriteTimeout: time.Second * 1,
		DB:           0,
	}

	cache := icache.NewRedisClient(opts)

	err := cache.Close()

	assert.NoError(t, err, "[TestCacheRedisClientClose] Should not empty")
}
