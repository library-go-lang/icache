package icache_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/library-go-lang/icache"
	"gitlab.com/library-go-lang/icache/mocks"
)

func TestCacheRedisCreateSuccess(t *testing.T) {
	cmd := new(mocks.RCacheRedisErrorCmd)
	cmd.On("Err").
		Return(nil)

	client := new(mocks.RCacheRedisClient)

	client.On("Set", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(cmd)

	cache := icache.NewRedisCache(client, time.Second)

	id, err := cache.Create(context.Background(), []byte("tes"))

	assert.NoError(t, err, "[TestCacheRedisCreateSuccess] Should not error")
	assert.NotEmpty(t, id, "[TestCacheRedisCreateSuccess] Should not empty")
}

func TestCacheRedisCreateError(t *testing.T) {
	cmd := new(mocks.RCacheRedisErrorCmd)
	cmd.On("Err").
		Return(fmt.Errorf("tes"))

	client := new(mocks.RCacheRedisClient)

	client.On("Set", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(cmd)

	cache := icache.NewRedisCache(client, time.Second)

	id, err := cache.Create(context.Background(), []byte("tes"))

	assert.Error(t, err, "[TestCacheRedisCreateError] Should error")
	assert.Equal(t, fmt.Errorf("tes"), err, "[TestCacheRedisCreateError] Error should \"tes\"")
	assert.NotEmpty(t, id, "[TestCacheRedisCreateError] Should not empty")
}
