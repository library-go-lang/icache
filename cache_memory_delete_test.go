package icache_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/icache"
)

func TestCacheMemoryDelete(t *testing.T) {
	cache := icache.NewMemoryCache(time.Second)

	err := cache.Delete(context.Background(), "tes")

	assert.NoError(t, err, "[TestCacheMemoryDelete] Should not error")
}
