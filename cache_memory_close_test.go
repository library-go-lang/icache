package icache_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/icache"
)

func TestCacheMemoryClose(t *testing.T) {
	cache := icache.NewMemoryCache(time.Second)
	cache.Set(context.Background(), "tes", []byte("rcache-test"))

	err := cache.Close()

	assert.NoError(t, err, "[TestCacheMemoryClose] Should not error")
}
