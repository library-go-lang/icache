package icache

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/library-go-lang/ihelper"
)

type redisCache struct {
	client        RCacheRedisClient
	cacheDuration time.Duration
}

func NewRedisCache(client RCacheRedisClient, duration time.Duration) CacheCloser {
	return &redisCache{
		client:        client,
		cacheDuration: duration,
	}
}

func (r redisCache) Get(ctx context.Context, key string) (value []byte, e error) {
	value, e = r.client.Get(ctx, key).Bytes()
	if e == redis.Nil {
		e = ErrNotFound
		return
	}

	if len(value) == 0 {
		e = ErrNoContent
		return
	}

	return
}

func (r redisCache) Delete(ctx context.Context, key string) (e error) {
	e = r.client.Del(ctx, key).Err()
	return
}

func (r redisCache) Set(ctx context.Context, key string, value []byte) (e error) {
	e = r.SetWithDuration(ctx, key, value, r.cacheDuration)
	return
}

func (r redisCache) Create(ctx context.Context, value []byte) (key string, e error) {
	return r.CreateWithDuration(ctx, value, r.cacheDuration)
}

func (r redisCache) SetWithDuration(ctx context.Context, key string, value []byte, dur time.Duration) (e error) {
	e = r.client.Set(ctx, key, value, dur).Err()
	return
}

func (r redisCache) CreateWithDuration(ctx context.Context, value []byte, dur time.Duration) (key string, e error) {
	key = ihelper.GenerateObjectID()
	e = r.client.Set(ctx, key, value, dur).Err()
	return
}

func (r redisCache) Close() (err error) {
	err = r.client.Close()
	return
}
