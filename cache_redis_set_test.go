package icache_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/library-go-lang/icache"
	"gitlab.com/library-go-lang/icache/mocks"
)

func TestCacheRedisSetSuccess(t *testing.T) {
	cmd := new(mocks.RCacheRedisErrorCmd)
	cmd.On("Err").
		Return(nil)

	client := new(mocks.RCacheRedisClient)

	client.On("Set", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(cmd)

	cache := icache.NewRedisCache(client, time.Second)

	err := cache.Set(context.Background(), "tes", []byte("tes"))

	assert.NoError(t, err, "[TestCacheRedisSetSuccess] Should not error")
}

func TestCacheRedisSetError(t *testing.T) {
	cmd := new(mocks.RCacheRedisErrorCmd)
	cmd.On("Err").
		Return(fmt.Errorf("tes"))

	client := new(mocks.RCacheRedisClient)

	client.On("Set", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(cmd)

	cache := icache.NewRedisCache(client, time.Second)

	err := cache.Set(context.Background(), "tes", []byte("tes"))

	assert.Error(t, err, "[TestCacheRedisSetError] Should error")
	assert.Equal(t, fmt.Errorf("tes"), err, "[TestCacheRedisSetError] Error should \"tes\"")
}
