package icache_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/icache"
)

func TestCacheMemoryGetSuccess(t *testing.T) {
	cache := icache.NewMemoryCache(time.Second)
	cache.Set(context.Background(), "tes", []byte("rcache-test"))

	b, err := cache.Get(context.Background(), "tes")

	assert.NoError(t, err, "[TestCacheMemoryGetSuccess] Should not error")
	assert.NotEmpty(t, b, "[TestCacheMemoryGetSuccess] Should not empty")
	assert.Equal(t, []byte("rcache-test"), b, "[TestCacheMemoryGetSuccess] Should equal []byte(\"rcache-test\")")
}

func TestCacheMemoryGetErrorNotFound(t *testing.T) {
	cache := icache.NewMemoryCache(time.Second)

	b, err := cache.Get(context.Background(), "tes")

	assert.Error(t, err, "[TestCacheMemoryGetErrorNotFound] Should error")
	assert.Equal(t, icache.ErrNotFound, err, "[TestCacheMemoryGetErrorNotFound] error should not found")
	assert.Empty(t, b, "[TestCacheMemoryGetErrorNotFound] Should empty")
}

func TestCacheMemoryGetErrorValueEmpty(t *testing.T) {
	cache := icache.NewMemoryCache(time.Second)
	cache.Set(context.Background(), "tes", []byte{})

	b, err := cache.Get(context.Background(), "tes")

	assert.Error(t, err, "[TestCacheMemoryGetErrorNotFound] Should error")
	assert.Equal(t, icache.ErrNoContent, err, "[TestCacheMemoryGetErrorNotFound] error should \"content is empty\"")
	assert.Empty(t, b, "[TestCacheMemoryGetErrorNotFound] Should empty")
}
