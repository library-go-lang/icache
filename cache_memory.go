package icache

import (
	"context"
	"sync"
	"time"

	"gitlab.com/library-go-lang/ihelper"
)

type memoryCache struct {
	mutex *sync.Mutex
	cache map[string][]byte
	durtn time.Duration
}

func NewMemoryCache(dur time.Duration) CacheCloser {
	return memoryCache{
		mutex: new(sync.Mutex),
		cache: make(map[string][]byte),
		durtn: dur,
	}
}

func (m memoryCache) Get(ctx context.Context, key string) (value []byte, err error) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	var isExist bool

	value, isExist = m.cache[key]
	if !isExist {
		err = ErrNotFound
		return
	}

	if len(value) == 0 {
		err = ErrNoContent
		return
	}

	return
}

func (m memoryCache) Delete(ctx context.Context, key string) (e error) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	delete(m.cache, key)
	return
}

func (m memoryCache) Set(ctx context.Context, key string, value []byte) (e error) {
	return m.SetWithDuration(ctx, key, value, m.durtn)
}

func (m memoryCache) Create(ctx context.Context, value []byte) (key string, e error) {
	return m.CreateWithDuration(ctx, value, m.durtn)
}

func (m memoryCache) SetWithDuration(ctx context.Context, key string, value []byte, dur time.Duration) (e error) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	m.cache[key] = value
	return
}

func (m memoryCache) CreateWithDuration(ctx context.Context, value []byte, dur time.Duration) (key string, e error) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	key = ihelper.GenerateObjectID()
	m.cache[key] = value
	return
}

func (m memoryCache) Close() (err error) {
	for k := range m.cache {
		delete(m.cache, k)
	}

	return
}
