package icache

import (
	"context"
	"time"
)

type RCacheRedisErrorCmd interface {
	Err() error
}

type RCacheRedisStatusCmd interface {
	RCacheRedisErrorCmd
	Bytes() ([]byte, error)
}

type RCacheRedisClient interface {
	Get(ctx context.Context, key string) RCacheRedisStatusCmd
	Del(ctx context.Context, keys ...string) RCacheRedisErrorCmd
	Set(ctx context.Context, key string, value interface{}, expiration time.Duration) RCacheRedisErrorCmd
	Close() error
}
