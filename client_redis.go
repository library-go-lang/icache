package icache

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
)

type RedisClient struct {
	client *redis.Client
}

func NewRedisClient(opts *redis.Options) RCacheRedisClient {
	rdb := redis.NewClient(opts)

	return &RedisClient{
		client: rdb,
	}
}

func (r RedisClient) Get(ctx context.Context, key string) RCacheRedisStatusCmd {
	return r.client.Get(ctx, key)
}

func (r RedisClient) Del(ctx context.Context, keys ...string) RCacheRedisErrorCmd {
	return r.client.Del(ctx, keys...)
}

func (r RedisClient) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) RCacheRedisErrorCmd {
	return r.client.Set(ctx, key, value, expiration)
}

func (r RedisClient) Close() error {
	return r.client.Close()
}
